import DenseNet
import DenseNet_input
import numpy as np
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

train, valid, test = DenseNet_input.getEmotionImage()

model = DenseNet.DenseNet(train, valid, test_data=test, input_size=42, num_class=7, init_lnr=1e-1, depth=40,
                          bc_mode=False, reduction=1.0, weight_decay=1e-4, total_blocks=3,
                          growth_rate=12, current_save_folder='./save/WideDenseNet-BC/', reduce_lnr=[70, 143, 180],
                          logs_folder='./summary/WideDenseNet-BC', valid_save_folder='./save/DenseNet2/valid/',
                          max_to_keep=0, snapshot_test=True)
score, label = model.get_score(batch_size=1, save_file=[0.6897, 0.6892, 0.6889])
label = np.argmax(np.array(label), axis=1)
score = TSNE(n_components=2).fit_transform(score)
score0 = score[label == 0, :]
score1 = score[label == 1, :]
score2 = score[label == 2, :]
score3 = score[label == 3, :]
score4 = score[label == 4, :]
score5 = score[label == 5, :]
score6 = score[label == 6, :]

# X_embedded = TSNE(n_components=2).fit_transform(score)
# print(X_embedded.shape)
plt.plot(score0[:,0], score0[:,1], 'bo')
plt.plot(score1[:,0], score1[:,1], 'go')
plt.plot(score2[:,0], score2[:,1], 'ro')
plt.plot(score3[:,0], score3[:,1], 'co')
plt.plot(score4[:,0], score4[:,1], 'mo')
plt.plot(score5[:,0], score5[:,1], 'yo')
plt.plot(score6[:,0], score6[:,1], 'ko')
plt.show()
