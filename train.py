from __future__ import print_function
import DenseNet
import DenseNet_input

train, test = DenseNet_input.getEmotionImage()

model = DenseNet.DenseNet(train, test, test_data=test, input_size=42, num_class=2, init_lnr=1e-1, depth=3 * 12 + 4,
                          bc_mode=False, reduction=1.0, weight_decay=1e-4, total_blocks=3, lnr_update_center=0.5,
                          weight_center_loss=0.0, growth_rate=12, reduce_lnr=[70, 143, 180],
                          current_save_folder='./save/DenseNet/', logs_folder='./summary/DenseNet',
                          valid_save_folder='./save/DenseNet/valid/',
                          max_to_keep=0, snapshot_test=False)
model.train(num_epoch=200, batch_size=128)
