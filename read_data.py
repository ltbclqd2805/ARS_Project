import numpy as np
import os
import cv2

SRC_PATH = './holes/'
DST_PATH = './processed_data/'

def make_one_hot(label):
    res = np.zeros(2)
    res[label] = 1.0
    return res


print('Reading and splitting data.................................')
data_ok = []
data_ng = []
all_data = []

ok_path = os.path.join(SRC_PATH, 'OK')
ng_path = os.path.join(SRC_PATH, 'NG')

for file in os.listdir(ng_path):
    img = cv2.imread(os.path.join(ng_path, file))
    img = cv2.resize(img, (48, 48))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = np.reshape(img, (48, 48, 1)) / 255.0
    label = make_one_hot(0)
    data_ng.append((img, label))
    all_data.append((img, label))

for file in os.listdir(ok_path):
    img = cv2.imread(os.path.join(ok_path, file))
    img = cv2.resize(img, (48, 48))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = np.reshape(img, (48, 48, 1)) / 255.0
    label = make_one_hot(1)
    data_ng.append((img, label))
    all_data.append((img, label))

np.random.shuffle(all_data)
num_samples = len(all_data)
train_data = all_data[:(int)(0.8 * num_samples)]
test_data = all_data[(int)(0.8 * num_samples):]

np.save(os.path.join(DST_PATH, 'train_data.npy'), train_data)
np.save(os.path.join(DST_PATH, 'test_data.npy'), test_data)


print('Done !')


